package ln2.flappybird;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.util.Log;
import android.view.View;

import java.util.Random;

/**
 * Created by NghiaTruongNgoc on 18/05/2017.
 */

public class Pipe {
    public Bitmap abovePipe; // the actual bitmap
    private Context context;
    public Bitmap belowPipe;
    private int x;   // the X coordinate
    private int y;   // the Y coordinate
    private Speed speed;
    private int abovePipePos = 0;
    public static int dstWidth = 0;
    public static int dstHeight = 0;

    private static final int spaceBetween = (int)(Resources.getSystem().getDisplayMetrics().heightPixels / 4);
    private static final int screenHeight = Resources.getSystem().getDisplayMetrics().heightPixels;
    private static final int screenWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
    public static int timeToRedraw = 2000;

    public Pipe(Bitmap abovePipe, Bitmap belowPipe) {
        this.abovePipe = abovePipe;
        this.belowPipe = belowPipe;
        speed = new Speed();
        x = screenWidth;
    }

    public int getBitmapHeight()
    {
        return dstHeight;
    }

    public static int getBitmapWidth()
    {
        return dstWidth;
    }

    public Speed getSpeed() {return speed;}

    public Bitmap getAbovePipe() {
        return abovePipe;
    }
    public void setBitmap(Bitmap abovePipe, Bitmap belowPipe) {
        this.abovePipe = abovePipe;
        this.belowPipe = belowPipe;
    }
    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }
    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }

    public int getAbovePipePos()
    {
        return abovePipePos;
    }

    public static int getSpaceBetween()
    {
        return spaceBetween;
    }

    public void draw(Canvas canvas) {
        int belowPipePos = abovePipePos + spaceBetween;
        canvas.drawBitmap(belowPipe, x, belowPipePos, null);
        canvas.drawBitmap(abovePipe, x, 0, null);
    }

    public void update() {
        x -= (speed.getXv() * speed.getxDirection()) * 6;
    }

    public static Pipe getNewPipe(Context context) {
        Random random = new Random();
        int abovePipePos = random.nextInt((int) (screenHeight / 1.5 - screenHeight / 4)) + screenHeight / 4;

        Log.d("nghia", String.valueOf(screenHeight));
        Log.d("nghia", String.valueOf(abovePipePos));


        Bitmap belowPipe = BitmapFactory.decodeResource(context.getResources(), R.drawable.belowpipe);
        Log.d("hu", String.valueOf(belowPipe.getHeight()));
        Log.d("hu", String.valueOf(belowPipe.getWidth()));

        double ratio = screenWidth / 3.5;
        Log.d("hu", String.valueOf(ratio));
        dstWidth = (int)(ratio);
        Log.d("hu", String.valueOf(dstWidth));
        dstHeight = (int)(belowPipe.getHeight() * (ratio / belowPipe.getWidth()));
        Log.d("hu", String.valueOf(dstHeight));

        Bitmap realBelowPipe = Bitmap.createScaledBitmap(
                belowPipe, dstWidth, dstHeight, true);

        Bitmap abovePipe = Bitmap.createScaledBitmap(
                BitmapFactory.decodeResource(context.getResources(), R.drawable.abovepipe),
                dstWidth, dstHeight, true);

        Bitmap realAbovePipe =Bitmap.createBitmap(abovePipe, 0, dstHeight - abovePipePos, dstWidth, abovePipePos);

        Log.d("nghia", String.valueOf(realAbovePipe.getHeight()));

        Pipe pipe = new Pipe(realAbovePipe, realBelowPipe);
        pipe.setAbovePipePos(abovePipePos);
        return pipe;
    }

    public void setAbovePipePos(int abovePipePos) {
        this.abovePipePos = abovePipePos;
    }
}
