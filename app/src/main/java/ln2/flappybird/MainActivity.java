package ln2.flappybird;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Color;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends Activity {
    /** Called when the activity is first created. */

    private static final String TAG = MainActivity.class.getSimpleName();
    private static MainGamePanel mainGamePanel;
    private GLSurfaceView glSurfaceView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // requesting to turn the title OFF
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        final FrameLayout game = new FrameLayout(this);
        // making it full screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // set our MainGamePanel as the View
        RelativeLayout gameWidgets = new RelativeLayout(this);
        final Button btn_Start = new Button(this);
        btn_Start.setWidth(300);
        btn_Start.setText("Start Game");

        final ImageView imageView = new ImageView(this);
        imageView.setImageResource(R.drawable.bird);
        gameWidgets.addView(btn_Start);
        gameWidgets.addView(imageView);

        final TextView txv_Score = new TextView(this);
        if (getIntent().hasExtra("SCORE")) {
            Bundle bundle = getIntent().getExtras();
            String strScore = bundle.getString("SCORE");
            if (!strScore.isEmpty() && strScore != null) {
                txv_Score.setText("Your score: " + strScore);
                gameWidgets.addView(txv_Score);
                txv_Score.setGravity(Gravity.CENTER | Gravity.BOTTOM);
                txv_Score.setTextColor(Color.parseColor("#ffffff"));
                txv_Score.setTextSize(20);
            }
        }

        mainGamePanel = new MainGamePanel(this);

        game.addView(mainGamePanel);
        game.addView(gameWidgets);
        //btn_Start.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        RelativeLayout.LayoutParams layoutParams =
                (RelativeLayout.LayoutParams)btn_Start.getLayoutParams();
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        btn_Start.setLayoutParams(layoutParams);

        RelativeLayout.LayoutParams layoutParamsI =
                (RelativeLayout.LayoutParams)imageView.getLayoutParams();
        layoutParamsI.addRule(RelativeLayout.CENTER_VERTICAL);
        imageView.setLayoutParams(layoutParamsI);
//
        if (txv_Score.getLayoutParams() != null) {
            RelativeLayout.LayoutParams layoutParamsTxv =
                    (RelativeLayout.LayoutParams) txv_Score.getLayoutParams();
            layoutParamsTxv.addRule(RelativeLayout.CENTER_HORIZONTAL);
            txv_Score.setLayoutParams(layoutParamsTxv);
        }
        setContentView(game);
        Log.d(TAG, "View added");
        btn_Start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txv_Score.setVisibility(View.GONE);
                btn_Start.setVisibility(View.GONE);
                imageView.setVisibility(View.GONE);
                mainGamePanel.onStartGame();
            }
        });


    }

    @Override
    protected void onDestroy() {
        mainGamePanel.pipes.clear();
        Log.d(TAG, "Destroying...");
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        mainGamePanel.pipes.clear();
        Log.d(TAG, "Stopping...");
        super.onStop();
    }
}