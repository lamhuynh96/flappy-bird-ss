package ln2.flappybird;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;

/**
 * Created by NghiaTruongNgoc on 18/05/2017.
 */

public class Bird {
    private Bitmap bitmap; // the actual bitmap
    private static int x;   // the X coordinate
    private int y;   // the Y coordinate
    private boolean touched; // if droid is touched/picked up
    private Speed speed;

    public static int getX()
    {
        return x;
    }

    public Bird(Bitmap bitmap, int x, int y) {
        this.bitmap = bitmap;
        this.x = x;
        this.y = y;
        speed = new Speed();
    }

    public Speed getSpeed() {return speed;}

    public Bitmap getBitmap() {
        return bitmap;
    }
    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
    public void setX(int x) {
        this.x = x;
    }
    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }

    public boolean isTouched() {
        return touched;
    }

    public void setTouched(boolean touched) {
        this.touched = touched;
    }

    public void draw(Canvas canvas) {
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        canvas.drawBitmap(bitmap, x - (bitmap.getWidth() / 2), y - (bitmap.getHeight() / 2), paint);
    }

    public int getWidthBird()
    {
        return bitmap.getWidth();
    }

    public int getHeightBird()
    {
        return bitmap.getHeight();
    }




    public void update() {
        if (!touched) {
            //x += (speed.getXv() * speed.getxDirection());
            speed.setYv(speed.getYv() + Speed.getSpeedTimeDecrease());
            y += (speed.getYv() * speed.getyDirection());

            if (speed.getYv() > Speed.getMaxSpeed())
            {
                speed.setYv(Speed.getMaxSpeed());
            }
        }
        else
        {
            speed.setYv(1);
            y -= 2.5 * (Speed.getDefaultSpeed() * speed.getyDirection());
            if (y < bitmap.getHeight())
                y = bitmap.getHeight();
        }
    }
}
