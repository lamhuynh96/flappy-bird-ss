package ln2.flappybird;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;

/**
 * Created by NghiaTruongNgoc on 17/05/2017.
 */

public class MainGamePanel extends SurfaceView implements
        SurfaceHolder.Callback {

    private final MyGLRenderer mRenderer;

    private static final String TAG = "MainPanel";

    private MainThread thread;
    private Bird bird;
    public ArrayList<Pipe> pipes;
    public static boolean toDrawPipe = true;
    private boolean passPipe = true;
    Context context;
    int score = 0;

    public static final int screenHeight = Resources.getSystem().getDisplayMetrics().heightPixels;
    public static final int screenWidth = Resources.getSystem().getDisplayMetrics().widthPixels;

    public MainGamePanel(Context context) {
        super(context);
        this.context = context;
        getHolder().addCallback(this);

        mRenderer = new MyGLRenderer();
        //setRenderer(mRenderer);

        pipes = new ArrayList<>();

        bird = new Bird(BitmapFactory.decodeResource(getResources(), R.drawable.bird), 150, (int)(screenHeight/2.5));
        // create the game loop thread

        setFocusable(true);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    public void onStartGame()
    {
        thread = new MainThread(getHolder(), this);
        thread.setRunning(true);
        thread.start();
    }

    public void onStopGame()
    {
        Log.d("nghia", "hh");
        if (thread != null) {
            thread.setRunning(false);
            pipes.clear();
            thread.timer.cancel();
            thread.timer.purge();

            thread.interrupt();
            thread = null;

            //start main
            Bundle bundle = new Bundle();
//Add your data from getFactualResults method to bundle
            bundle.putString("SCORE", String.valueOf(score));
//Add the bundle to the intent
            Intent intent = new Intent(getContext(), MainActivity.class);
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        while (retry) {
            try {
                if (thread != null)
                    thread.join();
                retry = false;
            } catch (InterruptedException e) {
                // try again shutting down the thread
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // fills the canvas with black
        canvas.drawColor(Color.BLACK);
        bird.draw(canvas);
    }

    protected void render(Canvas canvas) {
        // fills the canvas with black
        canvas.drawColor(Color.BLACK);
        bird.draw(canvas);

        for (int i = 0; i < pipes.size(); i++)
        {
            pipes.get(i).draw(canvas);
        }

        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.FILL);
        paint.setTextSize(100);
        canvas.drawText(String.valueOf(score), screenWidth - 100, 100, paint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            bird.setTouched(true);
        }

        if (event.getAction() == MotionEvent.ACTION_UP) {
            // touch was released
            if (bird.isTouched()) {
                bird.setTouched(false);
            }
        }
        return true;
    }

    public void update() {
        if (bird.getSpeed().getyDirection() == Speed.DIRECTION_DOWN
                && bird.getY() + bird.getBitmap().getHeight() / 2 >= getHeight()) {
            //bird.getSpeed().toggleYDirection();
            onStopGame();
            return;
        }
        // check collision with top wall if heading up
        if (bird.getSpeed().getyDirection() == Speed.DIRECTION_UP
                && bird.getY() - bird.getBitmap().getHeight() / 2 <= 0) {
            //bird.getSpeed().toggleYDirection();
            bird.setY(bird.getBitmap().getHeight() / 2);
        }

        // Update the lone bird
        bird.update();
        //pipe.update();

        for (int i = 0; i < pipes.size(); i++)
        {
            pipes.get(i).update();
        }

        if (MainGamePanel.toDrawPipe)
        {
            Pipe pipe = Pipe.getNewPipe(context);
            pipes.add(pipe);
            Log.d("pipe", String.valueOf(pipes.size()));
            MainGamePanel.toDrawPipe = false;
        }

        if (pipes.size() > 0) {
            if (pipes.get(0).getX() + Pipe.getBitmapWidth() < Bird.getX() && passPipe) {
                score++;
                passPipe = false;
            }

            if ((pipes.get(0).getX() <= Bird.getX()
                    && pipes.get(0).getX() + Pipe.dstWidth >= Bird.getX())
                    && (bird.getY() - bird.getHeightBird() / 2 <= pipes.get(0).getAbovePipePos()
                    || bird.getY() + bird.getHeightBird() / 2 >= pipes.get(0).getAbovePipePos() + Pipe.getSpaceBetween()))
            {
                onStopGame();
                return;
            }
            if (pipes.get(0).getX() + pipes.get(0).getBitmapWidth() < 0) {
                pipes.remove(0);
                passPipe = true;
            }
        }
    }
}
